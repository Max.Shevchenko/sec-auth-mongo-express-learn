const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');


//models
const User = require('./models/user')


//setup start
const app = express();

app.use(bodyParser.json())

mongoose.Promise = global.Promise

mongoose.connect('mongodb://localhost:27017/auth')

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'))
//setup end

//logic

//signup
app.post('/api/user', (req, res) => {
    console.log(req.body)
    const user = new User({
        email: req.body.email,
        password: req.body.password
    })

    user.save((err, doc) => {
        if (err) res.status(400).send(err)

        res.status(200).send()
    })
})


//login

app.post('/api/user/login', (req, res) => {

    User.findOne({email: 'sdfklkn@gmail.com'}, (err, doc) => {

        if (err) throw err

        doc.comparePassword(req.body.password, (err, isMatch) => {
            if (err) throw err
            if (isMatch) {res.json({trueUser: isMatch})} else {
            res.json({trueUser: isMatch})}
        })
    })
})


//test

// User.findOneAndUpdate({email: 'sdfklkn@gmail.com'}, {email: 'new@gmail.com'}, {new: true}, (err, doc) => {
//     if (err) console.log(err);
//     console.log(doc);
// })


//for save hook to work UPDATE THROUGH "SAVE"!!!!

// User.findOne({email: 'new@gmail.com'}, (err, doc) => {
//     if (err) console.log(err);
//     doc.password = '123456789'
//     doc.save((err, doc) => {
//         if (err) console.log(err);
//         console.log(doc);
//     })
// })





//port config
const PORT = process.env.PORT || 3000

app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
})



